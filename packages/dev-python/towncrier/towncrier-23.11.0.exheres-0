# Copyright 2023 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pypi py-pep517 [ backend=hatchling entrypoints=[ ${PN} ] ]

SUMMARY="A utility to produce useful, summarized changelogs for your project"
DESCRIPTION="
Rather than reading the Git history, or having one single file which
developers all write to and produce merge conflicts, towncrier reads “news
fragments” which contain information useful to end users.
While the command line tool towncrier works on Python 3.7+ only, as long as
you don’t use any Python-specific affordances (like auto-detection of the
project version), it is usable with any project type on any platform."

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/click[python_abis:*(-)?]
        dev-python/incremental[python_abis:*(-)?]
        dev-python/Jinja2[python_abis:*(-)?]
        python_abis:3.8? (
            dev-python/importlib_resources[>=5][python_abis:3.8]
            dev-python/tomli[python_abis:3.8]
        )
        python_abis:3.9? (
            dev-python/importlib_resources[>=5][python_abis:3.9]
            dev-python/tomli[python_abis:3.9]
        )
        python_abis:3.10? ( dev-python/tomli[python_abis:3.10] )
"

# Seems to need nox (unwritten) and Twisted for tests
RESTRICT="test"


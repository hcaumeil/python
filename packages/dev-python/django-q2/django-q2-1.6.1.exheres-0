# Copyright 2023 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PN=${PN/-/_}
MY_PNV=${MY_PN}-${PV}

require pypi py-pep517 [ backend=poetry-core test=pytest work=${MY_PNV} ]

SUMMARY="A multiprocessing distributed task queue for Django"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/Django[>=3.2&<6][python_abis:*(-)?]
        dev-python/django-picklefield[>=3.1][python_abis:*(-)?]
        python_abis:3.8? ( dev-python/importlib_metadata[>=3.6][python_abis:3.8] )
        python_abis:3.9? ( dev-python/importlib_metadata[>=3.6][python_abis:3.9] )
    test:
        dev-python/blessed[>=1.19.1][python_abis:*(-)?]
        dev-python/boto3[>=1.24.92][python_abis:*(-)?]
        dev-python/croniter[>=1.3.7][python_abis:*(-)?]
        dev-python/django-redis[>=5.2.0][python_abis:*(-)?]
        dev-python/hiredis[>=2.0.0][python_abis:*(-)?]
        dev-python/iron-mq[>=0.9][python_abis:*(-)?]
        dev-python/psutil[>=5.9.2][python_abis:*(-)?]
        dev-python/pymongo[>=4.2.0][python_abis:*(-)?]
        dev-python/pytest-django[>=4.5.2][python_abis:*(-)?]
        dev-python/redis[>=4.3.4][python_abis:*(-)?]
        dev-python/setproctitle[>=1.3.2][python_abis:*(-)?]
"

# Unwritten test dependencies
RESTRICT="test"

test_one_multibuild() {
    export DJANGO_SETTINGS_MODULE=django_q.tests.settings

    PYTHONPATH="$(ls -d ${PWD}/build/lib*)" edo ${PYTHON} -m pytest --ignore
}


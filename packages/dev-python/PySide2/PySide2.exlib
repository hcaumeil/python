# Copyright 2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pypi python [ blacklist=2 multibuild=false ] cmake

MY_PN="pyside-setup-opensource-src"

export_exlib_phases src_prepare

SUMMARY="PySide2 is the official Python module from the Qt for Python project"

HOMEPAGE+=" https://wiki.qt.io/Qt_for_Python"
DOWNLOADS="
    mirror://qt/official_releases/QtForPython/${PN,,}/${PNV}-src/${MY_PN}-${PV}.tar.xz
"

LICENCES="FDL-1.3 GPL-2 GPL-3 LGPL-3"
SLOT="0"
MYOPTIONS="
    charts            [[ description = [ Build bindings for QtCharts ] ]]
    datavisualization [[ description = [ Build bindings for QtDataVisualization ] ]]
    multimedia        [[ description = [ Build bindings for QtMultimedia ] ]]
    positioning       [[ description = [ Build bindings for QtPositioning ] ]]
    quickcontrols     [[ description = [ Build bindings for QtQuickControls2 ] ]]
    script            [[ description = [ Build bindings for QtScript ] ]]
    sensors           [[ description = [ Build bindings for QtSensors ] ]]
    serialport        [[ description = [ Build bindings for QtSerialPort ] ]]
    svg               [[ description = [ Build bindings for QtSvg ] ]]
    tts               [[ description = [ Build bindings for QtTextToSpeech ] ]]
    webchannel        [[ description = [ Build bindings for QtWebChannel ] ]]
    webengine         [[ description = [ Build bindings for QtWebEngine ] ]]
    websockets        [[ description = [ Build bindings for QtWebSockets ] ]]

    webengine  [[ requires = [ webchannel ] ]]
"

QT_VERSION="$(ever range 1-2)"

DEPENDENCIES="
    build+run:
        dev-python/shiboken2[python_abis:*(-)?]
        x11-libs/qtbase:5[~>${QT_VERSION}][sql]
        x11-libs/qtdeclarative:5[~>${QT_VERSION}] [[ note = [ could be optional ] ]]
        x11-libs/qttools:5[~>${QT_VERSION}] [[ note = [ could be optional ] ]]
        x11-libs/qtx11extras:5[~>${QT_VERSION}]
        x11-libs/qtxmlpatterns:5[~>${QT_VERSION}] [[
            note = [ optional, but needed by dev-python/shiboken2 ]
        ]]
        charts? ( x11-libs/qtcharts:5[~>${QT_VERSION}] )
        datavisualization? ( x11-libs/qtdatavis3d:5[~>${QT_VERSION}] )
        multimedia? ( x11-libs/qtmultimedia:5[~>${QT_VERSION}] )
        positioning? ( x11-libs/qtlocation:5[~>${QT_VERSION}] )
        quickcontrols? ( x11-libs/qtquickcontrols2:5[~>${QT_VERSION}] )
        script? ( x11-libs/qtscript:5[~>${QT_VERSION}] )
        sensors? ( x11-libs/qtsensors:5[~>${QT_VERSION}] )
        serialport? ( x11-libs/qtserialport:5[~>${QT_VERSION}] )
        svg? ( x11-libs/qtsvg:5[~>${QT_VERSION}] )
        tts? ( x11-libs/qtspeech:5[~>${QT_VERSION}] )
        webchannel? ( x11-libs/qtwebchannel:5[~>${QT_VERSION}] )
        webengine? ( x11-libs/qtwebengine:5[~>${QT_VERSION}] )
        websockets? ( x11-libs/qtwebsockets:5[~>${QT_VERSION}] )
"

# Many tests need a running X server
RESTRICT="test"

CMAKE_SOURCE="${WORKBASE}"/${MY_PN}-$(ever range -3)/sources/${PN,,}

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DPYTHON_EXECUTABLE=${PYTHON}
    -DCMAKE_DISABLE_FIND_PACKAGE_Qt53DAnimation:BOOL=TRUE
    -DCMAKE_DISABLE_FIND_PACKAGE_Qt53DCore:BOOL=TRUE
    -DCMAKE_DISABLE_FIND_PACKAGE_Qt53DExtras:BOOL=TRUE
    -DCMAKE_DISABLE_FIND_PACKAGE_Qt53DInput:BOOL=TRUE
    -DCMAKE_DISABLE_FIND_PACKAGE_Qt53DLogic:BOOL=TRUE
    -DCMAKE_DISABLE_FIND_PACKAGE_Qt53DRender:BOOL=TRUE
    -DCMAKE_DISABLE_FIND_PACKAGE_Qt5Charts:BOOL=TRUE
    -DCMAKE_DISABLE_FIND_PACKAGE_Qt5RemoteObjects:BOOL=TRUE
    -DCMAKE_DISABLE_FIND_PACKAGE_Qt5Scxml:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'charts Qt5Charts'
    'datavisualization Qt5DataVisualization'
    'multimedia Qt5Multimedia'
    'multimedia Qt5MultimediaWidgets'
    'positioning Qt5Location'
    'positioning Qt5Positioning'
    'quickcontrols Qt5QuickControls2'
    'script Qt5Script'
    'script Qt5ScriptTools'
    'sensors Qt5Sensors'
    'serialport Qt5SerialPort'
    'svg Qt5Svg'
    'tts Qt5TextToSpeech'
    'webchannel Qt5WebChannel'
    'webengine Qt5WebEngine'
    'webengine Qt5WebEngineCore'
    'webengine Qt5WebEngineWidgets'
    'websockets Qt5WebSockets'
)
CMAKE_SRC_CONFIGURE_TESTS+=(
    '-DBUILD_TESTS:BOOL=TRUE -DBUILD_TESTS:BOOL=FALSE'
)

PySide2_src_prepare() {
    cmake_src_prepare

    edo sed -e "s:\(DESTINATION \)\(share\):\1/usr/\2:" \
        -i cmake/Macros/PySideModules.cmake \
        -i PySide2/CMakeLists.txt

    edo sed -e 's:"${CMAKE_INSTALL_PREFIX}/share:"/usr/share:' \
        -i libpyside/CMakeLists.txt
}


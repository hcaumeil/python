# Copyright 2010-2017 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'lxml-2.1.1.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation

if ever at_least 5.1.0 ; then
    require pypi setup-py [ import=setuptools blacklist=2 ]
else
    require pypi setup-py [ import=setuptools ]
fi
require flag-o-matic

export_exlib_phases pkg_setup

SUMMARY="A Python binding for the libxml2 and libxslt libraries"
DESCRIPTION="
lxml is a Python binding for the libxml2 and libxslt XML processing libraries.
It aims at ease of use of the API for Python programmers and exposing the many
libxml2 features. It implements the Python ElementTree API on top of libxml2. It
extends this with support for XPath, XSLT, Relax NG, XML schema, and more.
"
HOMEPAGE="https://${PN}.de"

UPSTREAM_CHANGELOG="${HOMEPAGE}/changes-${PV}.html"
UPSTREAM_DOCUMENTATION="${HOMEPAGE}/index.html#documentation [[ lang = en ]]"

LICENCES="BSD-3"
SLOT="0"
MYOPTIONS="doc examples"

# The tests require an already-installed lxml...
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-python/Cython[>=0.29.37][python_abis:*(-)?]
    build+run:
        dev-libs/libxml2:2.0[>=2.9.2]
        dev-libs/libxslt[>=1.1.27]
        dev-python/beautifulsoup4[python_abis:*(-)?]
        dev-python/cssselect[>=0.7][python_abis:*(-)?]
        dev-python/html5lib[python_abis:*(-)?]
"

if ever at_least 5.1.0 ; then
    DEPENDENCIES+="
        build:
            dev-python/Cython[>=3.0.7][python_abis:*(-)?]
    "
fi

DISTUTILS_SRC_COMPILE_PARAMS=( --with-cython )

lxml_pkg_setup() {
    # Workaround to fix the build with gcc:14, actually a bug in Cython
    # https://bugs.launchpad.net/lxml/+bug/2045435
    # https://github.com/cython/cython/issues/2747
    if test-flag-CC -Wno-error=incompatible-pointer-types ; then
        append-flags -Wno-error=incompatible-pointer-types
    fi
}

test_one_multibuild() {
    edo "${PYTHON}" setup.py build_ext -i --with-cython

    export PYTHONPATH="${PYTHONPATH}:${WORK}/src"
    edo "${PYTHON}" selftest.py
    edo "${PYTHON}" selftest2.py
}

install_one_multibuild() {
    setup-py_install_one_multibuild

    if option doc; then
        insinto /usr/share/doc/${PNVR}/html
        doins -r doc/html/*
        dodoc *.txt
        docinto doc
        dodoc doc/*.txt
    fi

    if option examples; then
        insinto /usr/share/doc/${PNVR}/examples
        doins -r samples/*
    fi
}


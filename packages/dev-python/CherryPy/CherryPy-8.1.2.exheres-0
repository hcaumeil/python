# Copyright 2010-2015 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pypi setup-py [ blacklist=2 import=setuptools test=nose ]

SUMMARY="${PN} is a pythonic, object-oriented HTTP framework"
DESCRIPTION="
${PN} allows developers to build web applications in much the same way they would
build any other object-oriented Python program. This results in smaller source
code developed in less time.
${PN} is now more than ten years old and it is has proven very fast and stable.
It is being used in production by many sites, from the simplest ones to the most
demanding ones.
"
BASE_URI="${PN,,}.org"
HOMEPAGE="http://www.${BASE_URI}"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

# TODO filter out tests that require access to internet
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-python/setuptools_scm:0[python_abis:*(-)?]
    build+run:
        dev-python/six[python_abis:*(-)?]
    recommendation:
        dev-python/pyopenssl[python_abis:*(-)?] [[ description = [ HTTPS support ] ]]
"

SETUP_PY_SRC_INSTALL_PARAMS=( --install-data=/usr/share )

install_one_multibuild() {
    # FIXME: If cherryd exists it won't get reinstalled and the shebang will be wrong
    if [[ -e ${IMAGE}/usr/$(exhost --target)/bin/cherryd ]]; then
        edo rm "${IMAGE}"/usr/$(exhost --target)/bin/cherryd
        setup-py_install_one_multibuild
        [[ -e ${IMAGE}/usr/$(exhost --target)/bin/cherryd ]] || die "cherryd was not installed"
    else
        setup-py_install_one_multibuild
    fi

    # remove empty directory
    edo rmdir "${IMAGE}"/usr/share/cherrypy/process
}


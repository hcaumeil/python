# Copyright 2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PN=${PN/-/.}
MY_PNV=${MY_PN}-${PV}

require pypi
require setup-py [ import=setuptools blacklist='2' work=${MY_PNV} ]

SUMMARY="A client library for lazr.restful web services"
DESCRIPTION="
A programmable client library that takes advantage of the commonalities among
lazr.restful web services to provide added functionality on top of wadllib."

LICENCES="LGPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/distro[python_abis:*(-)?]
        dev-python/httplib2[>=0.7.7][python_abis:*(-)?]
        dev-python/oauthlib[python_abis:*(-)?]
        dev-python/six[python_abis:*(-)?]
        dev-python/wadllib[>=1.1.4][python_abis:*(-)?]
    test:
        dev-python/fixtures[>=1.3.0][python_abis:*(-)?]
        dev-python/lazr-authentication[python_abis:*(-)?]
        dev-python/oauth[python_abis:*(-)?]
        dev-python/testtools[python_abis:*(-)?]
        dev-python/wsgi_intercept[python_abis:*(-)?]
        dev-python/zopetestrunner[python_abis:*(-)?]
"

# Unwritten test dependencies
RESTRICT="test"

test_one_multibuild() {
    # test_not_stackable fails and starts pdb
    PYTHONPATH="$(ls -d ${PWD}/build/lib*)" \
        edo ${PYTHON} -m zope.testrunner -vc \
        --test-path src --tests-pattern ^tests
}

